var urlParams = new URLSearchParams(window.location.search);
var wordToFind = urlParams.has('name') ? urlParams.get('name') : 'Lennox';
var fill = d3.scale.category20();
var layout;
var closeTimer;

function clear() {
	// Clear things out if there was something there before.
	var cloud = document.getElementById('cloud');
	if (cloud) {
		cloud.innerHTML = '';
	}
}

function draw(words) {
	clear();
	d3.select('#cloud') //.append("svg")
		.attr("width", layout.size()[0])
		.attr("height", layout.size()[1])
		.append("g")
		.attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
		.selectAll("text")
		.data(words)
		.enter().append("text")
		.style("font-size", function(d) { return d.size + "px"; })
		.style("font-family", "Impact")
		.style("fill", function(d, i) { return fill(i); })
		.attr("text-anchor", "middle")
		.attr("transform", function(d) {
			return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
		})
		.text(function(d) { return d.text; });
}

function clearTimer () {
	if (closeTimer) {
		window.clearTimeout(closeTimer);
	}
}

function toggle (elements) {
	for (var i = 0; i < elements.length; i++) {
		var el = elements[i];
		if (el.classList.contains('hide')) {
			el.classList = [];
		} else {
			el.classList = ['hide'];
		}
	}
}

function closeMessage (fn) {
	clearTimer();
	
	var cloud = document.getElementById('cloud');
	var message = document.getElementById('message');
	var reload = document.getElementById('reload');

	if (cloud && message && reload) {
		// If there is a callback then perform it.
		if (fn) {
			fn();
		}

		toggle([cloud, reload, message]);
		message.innerText = '';
	}
}

function addMessage(text, className, fn) {
	var cloud = document.getElementById('cloud');
	var message = document.getElementById('message');
	var reload = document.getElementById('reload');

	if (cloud && message && reload) {
		// Temporarily hide the game.
		toggle([cloud, reload, message]);
		message.innerText = text;
		message.classList = [className];

		// Reset the visibility of the game after a set amount of time.
		closeTimer = window.setTimeout(function () {
			closeMessage(fn);
		}, 5 * 1000);

		// Reset the visibility of the game on click.
		message.onclick = function () {
			closeMessage(fn);
		}
	}
}

function correct() {
	addMessage('Winner!', 'correct', function () {
		clear();
		refresh();
	});
}

function incorrect() {
	addMessage('Oops, try again', 'incorrect');
}

function clickHandler () {
	var word = this.textContent;
	if (word === wordToFind) {
		correct();
	} else {
		incorrect();
	}
}

function addClickEvents () {
	var elements = document.getElementsByTagName('text');
	for (var i = 0; i < elements.length; i++) {
		elements[i].onclick = clickHandler;
	}
}

function dimensions () {
	return [document.documentElement.clientWidth - 20,
					document.documentElement.clientHeight - 20];
}

function handler (response) {
	response.push(wordToFind);
	layout = d3.layout.cloud()
		.size(dimensions())
		.words(response.map(function(d) {
			return {text: d, size: 10 + Math.random() * 90, test: "haha"};
		}))
		.padding(7)
		.rotate(function() { return ~~(Math.random() * 2) * 90; })
		.font("Impact")
		.fontSize(function(d) { return d.size; })
		.on("end", draw);
	layout.start();
	addClickEvents();
}

function refresh () {
	namey.get({ count: 10, with_surname: false, frequency: 'all', callback: handler });
}


(function () {
	var reload = document.getElementById('reload');
	reload.onclick = function () {
		refresh();
	}
	
	refresh();
})();
